# Sistema de Procesamiento y Análisis de Datos de Vigilancia Aeronáutica

## Descripción

Módulo que facilita la obtención de la información elemental de las aeronaves (e.g: posición en lat/long, altitud y altura, vector velocidad e identificación de la aeronave), así como figuras de mérito de su posición e información de la intensión de las mismas y otros datos de interés que puede obtener a través de los sistemas de vigilancia adb-s. Además permite generar estadísticas sobre las aeronaves que sobrevuelan la región Centroamericana. 

## Requisitos

Requiere el módulo [`framework`](http://github.com/silverstripe/silverstripe-framework) y la instalación del proyecto base [`silverstripe-installer`](http://github.com/silverstripe/silverstripe-installer).

## License ##

	Copyright (c) 2016, Raul Humberto Aguilar - raul.aguilar.01@gmail.com
	All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

	    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
	      documentation and/or other materials provided with the distribution.
	    * Neither the name of the copyright owner nor the names of its contributors may be used to endorse or promote products derived from this software 
	      without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
	GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
	STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
	OF SUCH DAMAGE.
